package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

class ForumBranchTests {

	@Test
	void withSinceDescLimitTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		ForumDAO forum = new ForumDAO(mockJdbc);
		ForumDAO.UserList("a", 1, "c", true);
		verify(mockJdbc).query(Mockito.eq(
				"SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
				Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
	}

	@Test
	void withoutSinceDescLimitTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		ForumDAO forum = new ForumDAO(mockJdbc);
		ForumDAO.UserList("a", null, null, false);
		verify(mockJdbc).query(Mockito.eq(
				"SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
				Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
	}

	@Test
	void withSinceWithoutDescLimitTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		ForumDAO forum = new ForumDAO(mockJdbc);
		ForumDAO.UserList("a", null, "b", false);
		verify(mockJdbc).query(Mockito.eq(
				"SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
				Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
	}

}