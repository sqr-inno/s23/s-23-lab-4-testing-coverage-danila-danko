package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import java.sql.Timestamp;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import com.hw.db.models.Post;

class PostBasisPathTests {

	private Post toch;
	private Timestamp tochTimestamp;

	@BeforeEach
	void createToch() {
		tochTimestamp = new Timestamp(0);
		toch = new Post();
		toch.setAuthor("a");
		toch.setMessage("a");
		toch.setCreated(tochTimestamp);
	}

	@Test
	void nullTest() {
		JdbcTemplate mock = mock(JdbcTemplate.class);
		Mockito.when(
				mock.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
				.thenReturn(toch);

		PostDAO postDAO = new PostDAO(mock);

		Timestamp postTimestamp = new Timestamp(0);
		Post post = new Post();
		post.setAuthor("a");
		post.setMessage("a");
		post.setCreated(postTimestamp);

		PostDAO.setPost(0, post);
		verify(mock, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
	}

	@Test
	void authorTest() {
		JdbcTemplate mock = mock(JdbcTemplate.class);
		Mockito.when(
				mock.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
				.thenReturn(toch);
		PostDAO postDAO = new PostDAO(mock);

		Timestamp postTimestamp = new Timestamp(0);
		Post post = new Post();
		post.setAuthor("b");
		post.setMessage("a");
		post.setCreated(postTimestamp);

		PostDAO.setPost(0, post);
		verify(mock).update(Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
				Optional.ofNullable(Mockito.any()));
	}

	@Test
	void messageTest() {
		JdbcTemplate mock = mock(JdbcTemplate.class);
		Mockito.when(
				mock.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
				.thenReturn(toch);
		PostDAO postDAO = new PostDAO(mock);

		Timestamp postTimestamp = new Timestamp(0);
		Post post = new Post();
		post.setAuthor("a");
		post.setMessage("b");
		post.setCreated(postTimestamp);

		PostDAO.setPost(0, post);
		verify(mock).update(Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
				Optional.ofNullable(Mockito.any()));

	}

	@Test
	void timestampTest() {
		JdbcTemplate mock = mock(JdbcTemplate.class);
		Mockito.when(
				mock.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
				.thenReturn(toch);
		PostDAO postDAO = new PostDAO(mock);

		Timestamp postTimestamp = new Timestamp(1);
		Post post = new Post();
		post.setAuthor("a");
		post.setMessage("a");
		post.setCreated(postTimestamp);

		PostDAO.setPost(0, post);
		verify(mock).update(
				Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
				Optional.ofNullable(Mockito.any()));

	}

	@Test
	void authorMessageTest() {
		JdbcTemplate mock = mock(JdbcTemplate.class);
		Mockito.when(
				mock.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
				.thenReturn(toch);
		PostDAO postDAO = new PostDAO(mock);

		Timestamp postTimestamp = new Timestamp(0);
		Post post = new Post();
		post.setAuthor("b");
		post.setMessage("b");
		post.setCreated(postTimestamp);

		PostDAO.setPost(0, post);
		verify(mock).update(
				Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
				Optional.ofNullable(Mockito.any()));

	}

	@Test
	void messageTimestampTest() {
		JdbcTemplate mock = mock(JdbcTemplate.class);
		Mockito.when(
				mock.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
				.thenReturn(toch);
		PostDAO postDAO = new PostDAO(mock);

		Timestamp postTimestamp = new Timestamp(1);
		Post post = new Post();
		post.setAuthor("a");
		post.setMessage("b");
		post.setCreated(postTimestamp);

		PostDAO.setPost(0, post);
		verify(mock).update(
				Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
				Optional.ofNullable(Mockito.any()));

	}

	@Test
	void authorMessageTimestampTest() {
		JdbcTemplate mock = mock(JdbcTemplate.class);
		Mockito.when(
				mock.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
				.thenReturn(toch);
		PostDAO postDAO = new PostDAO(mock);

		Timestamp postTimestamp = new Timestamp(1);
		Post post = new Post();
		post.setAuthor("b");
		post.setMessage("b");
		post.setCreated(postTimestamp);

		PostDAO.setPost(0, post);
		verify(mock).update(Mockito.eq(
				"UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
				Optional.ofNullable(Mockito.any()));

	}
}