package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import com.hw.db.DAO.PostDAO.PostMapper;

class ThreadStatementCoverageTests {

	@Test
	void test1() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		ThreadDAO thread = new ThreadDAO(mockJdbc);
		ThreadDAO.treeSort(1, 1, 1, true);
		verify(mockJdbc).query(Mockito.eq(
				"SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
				Mockito.any(PostMapper.class), Optional.ofNullable(Mockito.any()));
	}

	@Test
	void test2() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		ThreadDAO thread = new ThreadDAO(mockJdbc);
		ThreadDAO.treeSort(1, 1, 1, false);
		verify(mockJdbc).query(Mockito.eq(
				"SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
				Mockito.any(PostMapper.class), Optional.ofNullable(Mockito.any()));
	}

}