package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import com.hw.db.models.User;

class UserMCDCTests {

	@Test
	void nullTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		UserDAO user = new UserDAO(mockJdbc);
		UserDAO.Change(new User("a", null, null, null));
		verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
	}

	@Test
	void emailTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		UserDAO user = new UserDAO(mockJdbc);
		UserDAO.Change(new User("a", "b", null, null));
		verify(mockJdbc).update(Mockito.eq(
				"UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
				Mockito.eq("b"),
				Mockito.eq("a"));
	}

	@Test
	void fullnameTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		UserDAO user = new UserDAO(mockJdbc);
		UserDAO.Change(new User("a", null, "b", null));
		verify(mockJdbc).update(Mockito.eq(
				"UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
				Mockito.eq("b"),
				Mockito.eq("a"));
	}

	@Test
	void aboutTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		UserDAO user = new UserDAO(mockJdbc);
		UserDAO.Change(new User("a", null, null, "b"));
		verify(mockJdbc).update(Mockito.eq(
				"UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
				Mockito.eq("b"),
				Mockito.eq("a"));
	}

	@Test
	void emailFullNameTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		UserDAO user = new UserDAO(mockJdbc);
		UserDAO.Change(new User("a", "b", "c", null));
		verify(mockJdbc).update(Mockito.eq(
				"UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
				Mockito.eq("b"),
				Mockito.eq("c"),
				Mockito.eq("a"));
	}

	@Test
	void emailAboutTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		UserDAO user = new UserDAO(mockJdbc);
		UserDAO.Change(new User("a", "b", null, "c"));
		verify(mockJdbc).update(Mockito.eq(
				"UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
				Mockito.eq("b"),
				Mockito.eq("c"),
				Mockito.eq("a"));
	}

	@Test
	void fullnameAboutTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		UserDAO user = new UserDAO(mockJdbc);
		UserDAO.Change(new User("a", null, "b", "c"));
		verify(mockJdbc).update(Mockito.eq(
				"UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
				Mockito.eq("b"),
				Mockito.eq("c"),
				Mockito.eq("a"));
	}

	@Test
	void emailFullnameAboutTest() {
		JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
		UserDAO user = new UserDAO(mockJdbc);
		UserDAO.Change(new User("a", "b", "c", "d"));
		verify(mockJdbc).update(Mockito.eq(
				"UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
				Mockito.eq("b"),
				Mockito.eq("c"),
				Mockito.eq("d"),
				Mockito.eq("a"));
	}
}